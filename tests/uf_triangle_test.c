#include <stdint.h>

#include "debug.h"
#include "uf.h"

/*
 *    0 
 *   / \ 
 *  1---2
 */

int main() {
    int32_t us[3] = {0, 1, 2};
    int32_t vs[3] = {1, 2, 0};
    
    struct uf_t *uf = uf_union(us, vs, 3, 3);

    for(int32_t i = 0; i < 3; i++) {
        check(uf_find(uf, i) == uf_find(uf, (i + 1) % 3),
              "Coord %d and %d have different roots, %d and %d.\n",
              i, (i + 1) % 3, uf_find(uf, i), uf_find(uf, (i + 1) % 3));
    }

    for(int32_t i = 0; i < 3; i++) {
        check(uf_size(uf, i) == 3,
              "Coord %d has size %d when expecting size %d.\n",
              i, uf_size(uf, i), 3);
    }

    uf_free(uf);
}
