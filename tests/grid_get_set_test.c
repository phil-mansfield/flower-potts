#include <stdbool.h>
#include <stdint.h>

#include "grid.h"

int main()
{
    struct grid_t *gb = grid_newb(3, 3);
    struct grid_t *gf = grid_newf(3, 3);

    for(int32_t y; y < 3; y++) {
        for(int32_t x; x < 3; x++) {
            int32_t cf = grid_coord(gf, x, y);
            int32_t cb = grid_coord(gb, x, y);
            grid_idxf(gf, cf) = (float)cf;
            grid_idxb(gb, cb) = (uint8_t)cb;
        }
    }

    bool valid = true;
    for(int32_t y; y < 3; y++) {
        for(int32_t x; x < 3; x++) {
            int32_t c = grid_coord(gb, x, y);
            valid = valid && ((float) grid_idxb(gb, c) == grid_idxf(gf, c));
        }
    }

    grid_free(gf);
    grid_free(gb);
    return !valid;
}
