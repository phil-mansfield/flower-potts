#include <stdint.h>

#include "debug.h"
#include "uf.h"

#define MAX_COORD 5

int main() {
    struct uf_t *uf = uf_union(NULL, NULL, 0, MAX_COORD);

    for(int32_t i = 0; i < MAX_COORD; i++) {
        check(i == uf_find(uf, i), "Node %d had root %d. Expecting %d.\n",
              i, uf_find(uf, i), i);
    }

    uf_free(uf);
}
