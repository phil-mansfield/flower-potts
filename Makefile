CC=gcc
CFLAGS=-O2 -Wall -Wextra -Werror -std=c99

SOURCES=$(wildcard src/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))
HEADERS=$(patsubst %.c,%.o,$(SOURCES))

TARGET=build/libmcr.a
SO_TARGET=$(patsubst, %.a,%.so,$(TARGET))

TEST_SOURCES=$(wildcard tests/*.c)
TESTS=$(patsubst %.c,%,$(TEST_SOURCES))

all: $(TARGET) $(SO_TARGET)

debug: CFLAGS += -g -D DEBUG_MODE
debug: all

build:
	mkdir -p build/

grid.o:
uf.o: grid.h
mcr.o: grid.h mcr.h
%.o: %.c %.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(TARGET): build $(OBJECTS)
	ar rcs $@ $(OBJECTS)
	ranlib $@

$(SO_TARGET): $(CFLAGS) += -fPIC
$(SO_TARGET): build $(OBJECTS)
	$(CC) -shared -o $@ $(OBJECTS)

%_test: %_test.c $(TARGET)
	$(CC) $@.c -o $@ $(CFLAGS) -L build  -I src -lmcr
%_bench: %_bench.c $(TARGET)
	$(CC) $@.c -o $@ $(CFLAGS) -L build -I src -lmcr

tests: $(TESTS)
	@python tests/runTests.py tests/ test
	@python tests/runTests.py tests/ bench

clean:
	rm -rf tmp/ build/
	rm -f src/*.o
	rm -f tests/*_test
	rm -f tests/*_bench
