#ifndef GRID_H_
#define GRID_H_

#include <stdint.h>

/**********/
/* macros */
/**********/

#define grid_dx(g, coord, dx) /**/
#define grid_dy(g, coord, dy) /**/
#define grid_dx_dy(g, coord, dx, dy) /**/

#define grid_coord(g, x, y) ((x) + (y) * (g)->width)
#define grid_x(g, coord) ((coord) % (g)->width)
#define grid_y(g, coord) ((coord) / (g)->width)

#define grid_idx(type, g, coord) (((type*)(g)->data)[coord])
#define grid_idxf(g, coord) (grid_idx(float, g, coord))
#define grid_idxd(g, coord) (grid_idx(double, g, coord))
#define grid_idxi(g, coord) (grid_idx(int32_t, g, coord))
#define grid_idxl(g, coord) (grid_idx(int64_t, g, coord))
#define grid_idxb(g, coord) (grid_idx(uint8_t, g, coord))

#define grid_new(width, height, type) \
    (grid_new_sized(width, height, sizeof(type)))
#define grid_newf(width, height) (grid_new(width, height, float))
#define grid_newd(width, height) (grid_new(width, height, double))
#define grid_newi(width, height) (grid_new(width, height, uint32_t))
#define grid_newl(width, height) (grid_new(width, height, uint64_t))
#define grid_newb(width, height) (grid_new(width, height, uint8_t))

/*************/
/* datatypes */
/*************/

struct grid_t { 
    void *data;
    uint64_t elem_size;
    int32_t size;
    int32_t width;
    int32_t height;
};

/**************/
/* allocation */
/**************/

struct grid_t *
grid_new_sized(int32_t width,
               int32_t height,
               uint64_t elem_size);

void
grid_free(struct grid_t *g);

/********************/
/* getter functions */
/********************/

uint64_t
grid_get_elem_size(struct grid_t *g);

int32_t
grid_get_height(struct grid_t *g);

int32_t
grid_get_size(struct grid_t *g);

int32_t
grid_get_width(struct grid_t *g);

#endif /* GRID_H_ */
