#include <stdlib.h>

#include "debug.h"
#include "mcr.h"

/* Not even close to being fully implemented. */

/******************/
/* core functions */
/******************/

struct mcr_lattice_t
*mcr_new(int32_t width,
         mcr_spin_t spin_states,
         mcr_spin_t initial_spin_state)
{
    debug_assert(width > 1);

    (void) width;
    (void) spin_states;
    (void) initial_spin_state;

    return NULL;
}


void 
mcr_free(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;
}

void 
mcr_sweep(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;
}


void
mcr_renormalize(struct mcr_lattice_t *src_lat,
                struct mcr_lattice_t *dst_lat,
                enum mcr_renormalization_t renorm)
{
    debug_assert(src_lat);
    debug_assert(dst_lat);

    (void) src_lat;
    (void) dst_lat;
    (void) renorm;
}

double
mcr_correlation_strength(struct mcr_lattice_t *lat,
                         enum mcr_interaction_t intr)
{
    debug_assert(lat);
    
    (void) lat;
    (void) intr;

    return 0.;
}

double
mcr_interaction_energy(struct mcr_lattice_t *lat,
                       enum mcr_interaction_t intr)
{
    debug_assert(lat);

    (void) lat;
    (void) intr;

    return 0.;
}

double
mcr_total_energy(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;

    return 0.;
}

void
mcr_print(struct mcr_lattice_t *lat)
{
    debug_assert(lat);
    
    (void) lat;
}

/***********************/
/* histogram functions */
/***********************/

struct mcr_histogram_t *
mcr_generate_hist(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;

    return NULL;
}

void
mcr_reset_hist(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;
}

double
mcr_hist_min_energy(struct mcr_histogram_t *hist)
{
    debug_assert(hist);

    (void) hist;

    return 0.;
}

double
mcr_hist_energy_step(struct mcr_histogram_t *hist)
{
    debug_assert(hist);

    (void) hist;

    return 0.;
}

int32_t
mcr_hist_size(struct mcr_histogram_t *hist)
{
    debug_assert(hist);

    (void) hist;

    return 0;
}

int32_t *
mcr_hist_bins(struct mcr_histogram_t *hist)
{
    debug_assert(hist);

    (void) hist;

    return NULL;
}

double
mcr_hist_energy(struct mcr_histogram_t *hist,
                double temp)
{
    debug_assert(hist);
    debug_assert(temp >= 0.);

    (void) hist;
    (void) temp;

    return 0.;
}

double
mcr_hist_mag(struct mcr_histogram_t *hist,
             double temp)
{
    debug_assert(hist);
    debug_assert(temp >= 0.);

    (void) hist;
    (void) temp;

    return 0.;
}

/********************/
/* setter functions */
/********************/

void
mcr_set_temperature(struct mcr_lattice_t *lat,
                    float temp)
{
    debug_assert(lat);
    debug_assert(temp >= 0.);

    (void) lat;
    (void) temp;
}

void
mcr_set_interactions(struct mcr_lattice_t *lat,
                     enum mcr_interaction_t *interaction_ts,
                     float *strengths,
                     int32_t interaction_count)
{
    debug_assert(lat);
    debug_assert(interaction_ts);
    debug_assert(strengths);
    debug_assert(interaction_count >= 0);

    (void) lat;
    (void) interaction_ts;
    (void) strengths;
    (void) interaction_count;
}

/********************/
/* getter functions */
/********************/

int32_t
mcr_get_width(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;

    return 0;
}

int32_t
mcr_get_sites(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;

    return 0.;
}

float
mcr_get_temperature(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;

    return 0.;
}

int32_t
mcr_get_interaction_count(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;

    return 0;
}

enum mcr_interaction_t *
mcr_get_interaction_ts(struct mcr_lattice_t *lat)
{
    debug_assert(lat);
    
    (void) lat;

    return NULL;
}

float *
mcr_get_strengths(struct mcr_lattice_t *lat)
{
    debug_assert(lat);

    (void) lat;

    return NULL;
}
