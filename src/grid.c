#include <stdlib.h>

#include "debug.h"
#include "grid.h"

struct grid_t *
grid_new_sized(int32_t width, 
               int32_t height,
               uint64_t elem_size)
{
    debug_assert(width > 0);
    debug_assert(height > 0);
    debug_assert(elem_size > 0);

    struct grid_t *g = malloc(sizeof(*g));
    check_mem(g);

    g->width = width;
    g->height = height;
    g->size = height * width;

    g->data = malloc(sizeof(elem_size) *g->size);
    check_mem(g->data);

    return g;
}

void
grid_free(struct grid_t *g)
{
    debug_assert(g);

    free(g->data);
    free(g);
}

uint64_t
grid_get_elem_size(struct grid_t *g)
{
    debug_assert(g);

    return g->elem_size;
}

int32_t
grid_get_height(struct grid_t *g)
{
    debug_assert(g);

    return g->height;
}

int32_t
grid_get_size(struct grid_t *g)
{
    debug_assert(g);

    return g->size;
}

int32_t
grid_get_width(struct grid_t *g)
{
    debug_assert(g);

    return g->width;
}
