#ifndef __UNION_FIND_H__
#define __UNION_FIND_H__

#include <stdint.h>

struct uf_t {
    int32_t *us, *vs;
    int32_t *roots, *sizes;
    int32_t edge_count;
    int32_t coord_count; /* This is perhaps a misnomer? */
};

struct uf_t *
uf_union(int32_t *us,
         int32_t *vs,
         int32_t edge_count,
         int32_t max_coord);
void 
uf_free(struct uf_t *uf);

int32_t
uf_find(struct uf_t *uf,
        int32_t coord);
int32_t
uf_size(struct uf_t *uf,
        int32_t coord);

#endif /* __UNION_FIND_H__ */
