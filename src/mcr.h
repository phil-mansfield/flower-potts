#ifndef __MCR_H__
#define __MCR_H__

#include <stdint.h>

#include "mcr.h"

/********************/
/* simulation types */
/********************/

typedef uint8_t mcr_spin_t;

struct mcr_lattice_t { /* fileds go here */ };
struct mcr_histogram_t { /* fields go here */ };


enum mcr_interaction_t {
    MCR_NN1, /* nearest neighbor */
    MCR_NN2, /* second nearest neighbor */
    MCR_NN3, /* third nearest neighbor */
    MCR_NN4, /* fourth nearest neighbor */
    MCR_SQR, /* plaquette */
    MCR_DMD  /* diamond */  
};
#define MCR_INTERACTION_COUNT 6

enum mcr_renormalization_t {
    MCR_RAND2
};

/**********************/
/* benchmarking types */
/**********************/

struct mcr_time_record_t { /* fields go here */ };

enum mcr_time_style_t {
  MCR_TIME_PER_SWEEP,
  MCR_TIME_PER_SIM
};

typedef uint32_t mcr_code_location_t;
/* Insert different locaiton names here. Each must be 1<<n flags. */

/******************/
/* core functions */
/******************/

struct mcr_lattice_t
*mcr_new(int32_t width,
         mcr_spin_t spin_states,
         mcr_spin_t initial_spin_state);
void 
mcr_free(struct mcr_lattice_t *lat);

void 
mcr_sweep(struct mcr_lattice_t *lattice);
void
mcr_renormalize(struct mcr_lattice_t *src_lat,
                struct mcr_lattice_t *dst_lat,
                enum mcr_renormalization_t renorm);

double
mcr_correlation_strength(struct mcr_lattice_t *lat,
                         enum mcr_interaction_t intr);
double
mcr_interaction_energy(struct mcr_lattice_t *lat,
                       enum mcr_interaction_t intr);
double
mcr_total_energy(struct mcr_lattice_t *lat);

void
mcr_print(struct mcr_lattice_t *lat);

/***********************/
/* histogram functions */
/***********************/

struct mcr_histogram_t *
mcr_generate_histogram(struct mcr_lattice_t *lat);
void
mcr_reset_histogram(struct mcr_lattice_t *lat);

double
mcr_histogram_min_energy(struct mcr_histogram_t *hist);
double
mc_histogram_energy_step(struct mcr_histogram_t *hist);
int32_t
mcr_histogram_size(struct mcr_histogram_t *hist);
int32_t *
mcr_histogram_bins(struct mcr_histogram_t *hist);

double
mcr_histogram_energy(struct mcr_histogram_t *hist,
                     double temp);
double
mcr_histogram_mag(struct mcr_histogram_t *hist,
                  double temp);

/********************/
/* setter functions */
/********************/

void
mcr_set_temperature(struct mcr_lattice_t *lat,
                    float temp);
void
mcr_set_interactions(struct mcr_lattice_t *lat,
                     enum mcr_interaction_t *interaction_ts,
                     float *strengths,
                     int32_t interaction_count);

/********************/
/* getter functions */
/********************/

int32_t
mcr_get_width(struct mcr_lattice_t *lat);
int32_t
mcr_get_sites(struct mcr_lattice_t *lat);
float
mcr_get_temperature(struct mcr_lattice_t *lat);
int32_t
mcr_get_interaction_count(struct mcr_lattice_t *lat);
enum mcr_interaction_t *
mcr_get_interaction_ts(struct mcr_lattice_t *lat);
float *
mcr_get_strengths(struct mcr_lattice_t *lat);

#endif /* __MCR_H__ */
