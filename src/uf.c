#include <stdlib.h>

#include "debug.h"
#include "uf.h"

struct uf_t *
new_union(int32_t *us,
          int32_t *vs,
          int32_t edge_count,
          int32_t coord_count)
{
    struct uf_t *uf = malloc(sizeof(*uf));
    check_mem(uf);

    uf->us = us;
    uf->vs = vs;

    uf->roots = malloc(sizeof(*uf->roots) * coord_count);
    check_mem(uf->roots);
    uf->sizes = malloc(sizeof(*uf->sizes) * coord_count);
    check_mem(uf->sizes);

    uf->edge_count = edge_count;
    uf->coord_count = coord_count;

    for(int32_t i = 0; i < coord_count; i++) {
        uf->roots[i] = i;
        uf->sizes[i] = 1;
    }

    return uf;
}

/* `uf_union()` creates a new instance of `struct uf_t` out of the given
 * arrays repreenting `edge_count` edges and `coord_count` nodes. If
 * `us[i] = a` and `vs[i] = b` then there will be a directionless edge
 * between nodes `a` and `b`. */
struct uf_t *
uf_union(int32_t *us,
         int32_t *vs,
         int32_t edge_count,
         int32_t coord_count)
{
    debug_assert(us || edge_count == 0);
    debug_assert(vs || edge_count == 0);
    debug_assert(edge_count >= 0);
    debug_assert(coord_count > 0);

    struct uf_t *uf = new_union(us, vs, edge_count, coord_count);

    for(int32_t i = 0; i < edge_count; i++) {
        int32_t uRoot = uf_find(uf, us[i]);
        int32_t vRoot = uf_find(uf, vs[i]);
        if(uRoot == vRoot)
            continue;

        int32_t root, leaf;
        if(uf->sizes[uRoot] > uf->sizes[vRoot]) {
            root = uRoot;
            leaf = vRoot;
        } else {
            root = vRoot;
            leaf = uRoot;
        }

        uf->sizes[root] = uf->sizes[root] + uf->sizes[leaf];
        uf->roots[leaf] = root;
    }

    return uf;
}

/* `uf_free()` frees the memory associated with `uf`. This does not
 * free `uf->us` or `uf->vs` since those may be stack allocated. */
void
uf_free(struct uf_t *uf)
{
    debug_assert(uf);

    free(uf->roots);
    free(uf->sizes);
    free(uf);
}

/* `uf_find()` returns the ID of the group which `coord` belongs to.
 * This ID is garuanteed to be one of the nodes in the group of
 * `coord`. */
int32_t
uf_find(struct uf_t *uf,
        int32_t coord)
{
    debug_assert(uf);
    debug_assert(coord >= 0);
    debug_assert(coord < uf->coord_count);

    int32_t root = coord;

    while(root != uf->roots[root]) {
        root = uf->roots[root];
    }

    while(uf->roots[coord] != coord) {
        int32_t old_root = uf->roots[coord];
        uf->roots[coord] = root;
        coord = old_root;
    }

    return root;
}

/* uf_size() returns the number of elements in the group of `coord`. */
int32_t
uf_size(struct uf_t *uf,
        int32_t coord)
{
    debug_assert(uf);
    debug_assert(coord >= 0);
    debug_assert(coord < uf->coord_count);

    return uf->sizes[uf_find(uf, coord)];
}

