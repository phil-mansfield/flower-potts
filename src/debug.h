#ifndef DEBUG_H_
#define DEBUG_H_

/* These macros are taken almost entirely from Zed A. Shaw's excellent Learn
 * C The Hard Way tutorial. */

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG_MODE
#define log_debug(M, ...) fprintf(stderr, "[DEBUG] %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define debug_assert(pred) assert(pred)
#else
#define log_debug(M, ...)
#define debug_assert(pred)
#endif /* DEBUG_MODE */

#define clean_errno() (errno == 0 ? "None" : strerror(errno))
#define log_err(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define log_warn(M, ...) fprintf(stderr, "[WARN] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)
#define log_info(M, ...) fprintf(stderr, "[INFO] (%s:%d) " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#define check(A, M, ...) if(!(A)) { log_err(M, ##__VA_ARGS__); exit(1); }
#define sentinel(M, ...)  { log_err(M, ##__VA_ARGS__); exit(1); }
#define check_mem(A) check((A), "Malloc has failed. Where is your God now?")

#endif /* DEBUG_H_ */
